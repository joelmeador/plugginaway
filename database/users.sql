/*
 Navicat PostgreSQL Data Transfer

 Source Server         : localhost
 Source Server Version : 90408
 Source Host           : localhost
 Source Database       : indy-ex-db
 Source Schema         : public

 Target Server Version : 90408
 File Encoding         : utf-8

 Date: 08/01/2016 01:12:46 AM
*/

-- ----------------------------
--  Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
	"id" bigserial NOT NULL,
	"detail" varchar(140) NOT NULL COLLATE "default",
	"secret_key" varchar(32) NOT NULL COLLATE "default",
	"secret_hash" varchar NOT NULL COLLATE "default",
	"enabled" bool NOT NULL DEFAULT true
)
WITH (OIDS=FALSE);
ALTER TABLE "public"."users" OWNER TO "postgres";

-- ----------------------------
--  Records of users
-- ----------------------------
BEGIN;
INSERT INTO "public"."users" VALUES ('1', 'Things That Make You Go Hmm', '1234567890abcdef1234567890abcdef', 'abc80ad9809d8f98e98098b9', 't');
INSERT INTO "public"."users" VALUES ('2', 'Here We Go (Let’s Rock & Roll)', '234567890abcdef1234567890abcdef1', 'bc80ad9809d8f98e98098b9a', 't');
INSERT INTO "public"."users" VALUES ('3', 'Gonna Make You Sweat (Everybody Dance Now)', '34567890abcdef1234567890abcdef12', 'c80ad9809d8f98e98098b9ab', 'f');
COMMIT;

-- ----------------------------
--  Primary key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table users
-- ----------------------------
CREATE INDEX  "index_users_on_enabled" ON "public"."users" USING btree(enabled "pg_catalog"."bool_ops" ASC NULLS LAST) WHERE enabled IS TRUE;
CREATE INDEX  "index_users_on_secret_hash" ON "public"."users" USING btree(secret_hash COLLATE "default" "pg_catalog"."text_ops" ASC NULLS LAST);
CREATE INDEX  "index_users_on_secret_key" ON "public"."users" USING btree(secret_key COLLATE "default" "pg_catalog"."text_ops" ASC NULLS LAST);
