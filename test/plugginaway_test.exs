defmodule PlugginawayTest do
  use ExUnit.Case, async: true
  use Plug.Test

  @opts Plugginaway.init([])

  test "returns conn" do
    # Create a test connection
    conn = conn(:get, "/")

    # Invoke the plug
    conn = Plugginaway.call(conn, @opts)

    # Assert the status
    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "Hello world"
    [_, {"content-type", content_type}] = conn.resp_headers
    assert content_type == "text/plain; charset=utf-8"
  end
end
